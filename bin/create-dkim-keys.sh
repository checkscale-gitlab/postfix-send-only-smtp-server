#!/bin/bash

set -e

source .env

DKIM_KEYS_PATH=$(realpath volume/opendkim/keys)

# create key
docker run --rm -it -v $DKIM_KEYS_PATH:/keys -w /keys instrumentisto/opendkim:alpine \
  opendkim-genkey -d $DOMAINNAME -s $SUBDOMAIN -D . -r

echo $DKIM_KEYS_PATH
ls -la $DKIM_KEYS_PATH
