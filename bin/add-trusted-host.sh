#!/bin/sh

set -e

DOCKER_NETWORK="$1.$(basename $PWD)_default"

echo $DOCKER_NETWORK >> ./volume/TrustedHosts
docker-compose restart
docker network connect $DOCKER_NETWORK $1
